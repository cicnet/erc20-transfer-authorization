# standard imports
import os
import unittest
import logging

# external imports
from chainlib.eth.nonce import RPCNonceOracle
from chainlib.eth.address import is_same_address
from chainlib.eth.tx import receipt
from giftable_erc20_token import GiftableToken
from eth_erc20 import ERC20

# local imports
from erc20_transfer_authorization import TransferAuthorization
from erc20_transfer_authorization.unittest import TestBase

logg = logging.getLogger()

testdir = os.path.dirname(__file__)


class TestTransfer(TestBase):

    def setUp(self):
        super(TestTransfer, self).setUp()
        nonce_oracle = RPCNonceOracle(self.accounts[0], self.rpc)
        self.c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)

        c = GiftableToken(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.mint_to(self.token_address, self.accounts[0], self.accounts[9], 10000)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[9], self.rpc)
        c = ERC20(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.approve(self.token_address, self.accounts[9], self.address, 1024)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)


    def test_premature_transfer(self):
        (tx_hash_hex, o) = self.c.create_request(self.address, self.accounts[0], self.accounts[9], self.accounts[2], self.token_address, 1024)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.check_result(self.address, self.accounts[0], 1)
        r = self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.execute_request(self.address, self.accounts[0], 1)
        r = self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 0)

        o = self.c.requests(self.address, 1, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        request = self.c.parse_request(r)
        self.assertTrue(request.is_voting())
        self.assertFalse(request.is_executed())


    def test_reject_transfer(self):
        (tx_hash_hex, o) = self.c.create_request(self.address, self.accounts[0], self.accounts[9], self.accounts[2], self.token_address, 1024)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.set_thresholds(self.address, self.accounts[0], 1, 1)
        self.rpc.do(o)

        (tx_hash_hex, o) = self.c.nay(self.address, self.accounts[0], 1)
        self.rpc.do(o)

        (tx_hash_hex, o) = self.c.check_result(self.address, self.accounts[0], 1)
        r = self.rpc.do(o)
       
        (tx_hash_hex, o) = self.c.execute_request(self.address, self.accounts[0], 1)
        r = self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 0)

        o = self.c.requests(self.address, 1, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        request = self.c.parse_request(r)
        self.assertTrue(request.is_rejected())
        self.assertFalse(request.is_voting())
        self.assertFalse(request.is_executed())


    def test_transfer(self):
        (tx_hash_hex, o) = self.c.create_request(self.address, self.accounts[0], self.accounts[9], self.accounts[2], self.token_address, 1024)
        self.rpc.do(o)
       
        (tx_hash_hex, o) = self.c.yay(self.address, self.accounts[0], 1)
        self.rpc.do(o)

        (tx_hash_hex, o) = self.c.check_result(self.address, self.accounts[0], 1)
        r = self.rpc.do(o)

        (tx_hash_hex, o) = self.c.execute_request(self.address, self.accounts[0], 1)
        r = self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        o = self.c.requests(self.address, 1, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        request = self.c.parse_request(r)
        logg.debug('request {}'.format(request))
        self.assertTrue(request.is_transferred())
        self.assertTrue(request.is_executed())

        nonce_oracle = RPCNonceOracle(self.accounts[0], self.rpc)
        c = ERC20(self.chain_spec, nonce_oracle=nonce_oracle)
        o = c.balance(self.token_address, self.accounts[9], sender_address=self.accounts[0])
        r = self.rpc.do(o)
        balance = c.parse_balance(r)
        self.assertEqual(balance, 10000-1024)

        (tx_hash_hex, o) = self.c.execute_request(self.address, self.accounts[0], 1)
        r = self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 0)


if __name__ == '__main__':
    unittest.main()
