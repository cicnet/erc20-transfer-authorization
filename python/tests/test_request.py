# standard imports
import os
import unittest
import logging
import random

# external imports
from chainlib.eth.nonce import RPCNonceOracle
from chainlib.eth.address import is_same_address
from giftable_erc20_token import GiftableToken
from eth_erc20 import ERC20
from chainlib.eth.tx import receipt

# local imports
from erc20_transfer_authorization import TransferAuthorization
from erc20_transfer_authorization.unittest import TestBase

#logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()

testdir = os.path.dirname(__file__)


class TestRequest(TestBase):

    def setUp(self):
        super(TestRequest, self).setUp()
        nonce_oracle = RPCNonceOracle(self.accounts[0], self.rpc)
        self.c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)

        c = GiftableToken(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.mint_to(self.token_address, self.accounts[0], self.accounts[9], 10000)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[9], self.rpc)
        c = ERC20(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.approve(self.token_address, self.accounts[9], self.address, 10000)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)


    def test_basic(self):
        o = self.c.next_serial(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        serial = self.c.parse_count(r)
        self.assertEqual(serial, 0)

        o = self.c.last_serial(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        serial = self.c.parse_count(r)
        self.assertEqual(serial, 0)

        (tx_hash_hex, o) = self.c.create_request(self.address, self.accounts[0], self.accounts[9], self.accounts[2], self.token_address, 1)
        self.rpc.do(o)

        (tx_hash_hex, o) = self.c.create_request(self.address, self.accounts[0], self.accounts[9], self.accounts[3], self.token_address, 2)
        self.rpc.do(o)

        o = self.c.last_serial(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        serial = self.c.parse_count(r)
        self.assertEqual(serial, 2)

        o = self.c.next_serial(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        serial = self.c.parse_count(r)
        self.assertEqual(serial, 1)

        (tx_hash_hex, o) = self.c.yay(self.address, self.accounts[0], 1)
        self.rpc.do(o)

        (tx_hash_hex, o) = self.c.check_result(self.address, self.accounts[0], 1)
        self.rpc.do(o)

        (tx_hash_hex, o) = self.c.execute_request(self.address, self.accounts[0], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        o = self.c.count(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        count = self.c.parse_count(r)
        self.assertEqual(count, 1)

        o = self.c.last_serial(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        serial = self.c.parse_count(r)
        self.assertEqual(serial, 2)

        o = self.c.next_serial(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        serial = self.c.parse_count(r)
        self.assertEqual(serial, 2)

        (tx_hash_hex, o) = self.c.nay(self.address, self.accounts[0], 2)
        self.rpc.do(o)

        (tx_hash_hex, o) = self.c.check_result(self.address, self.accounts[0], 2)
        self.rpc.do(o)

        o = self.c.count(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        count = self.c.parse_count(r)
        self.assertEqual(count, 0)

        o = self.c.last_serial(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        serial = self.c.parse_count(r)
        self.assertEqual(serial, 2)

        o = self.c.next_serial(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        serial = self.c.parse_count(r)
        self.assertEqual(serial, 0)


    def test_hi_lo(self):
        for i in range(5):
            (tx_hash_hex, o) = self.c.create_request(self.address, self.accounts[0], self.accounts[9], self.accounts[i+1], self.token_address, i+1)
            self.rpc.do(o)

        o = self.c.last_serial(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        serial = self.c.parse_count(r)
        self.assertEqual(serial, 5)

        o = self.c.next_serial(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        serial = self.c.parse_count(r)
        self.assertEqual(serial, 1)

        (tx_hash_hex, o) = self.c.yay(self.address, self.accounts[0], 6)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 0)

        (tx_hash_hex, o) = self.c.yay(self.address, self.accounts[0], 5)
        self.rpc.do(o)

        (tx_hash_hex, o) = self.c.check_result(self.address, self.accounts[0], 5)
        self.rpc.do(o)

        (tx_hash_hex, o) = self.c.execute_request(self.address, self.accounts[0], 5)
        self.rpc.do(o)

        o = self.c.last_serial(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        serial = self.c.parse_count(r)
        self.assertEqual(serial, 5)

        o = self.c.next_serial(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        serial = self.c.parse_count(r)
        self.assertEqual(serial, 1)

        o = self.c.requests(self.address, 5, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        request = self.c.parse_request(r)
        self.assertTrue(request.is_executed())

        o = self.c.count(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        count = self.c.parse_count(r)
        self.assertEqual(count, 4)
   
        (tx_hash_hex, o) = self.c.create_request(self.address, self.accounts[0], self.accounts[9], self.accounts[0], self.token_address, 100)
        self.rpc.do(o)

        o = self.c.count(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        count = self.c.parse_count(r)
        self.assertEqual(count, 5)

        o = self.c.last_serial(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        serial = self.c.parse_count(r)
        self.assertEqual(serial, 6)

        o = self.c.next_serial(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        serial = self.c.parse_count(r)
        self.assertEqual(serial, 1)

        (tx_hash_hex, o) = self.c.yay(self.address, self.accounts[0], 6)
        self.rpc.do(o)

        (tx_hash_hex, o) = self.c.check_result(self.address, self.accounts[0], 6)
        self.rpc.do(o)

        (tx_hash_hex, o) = self.c.execute_request(self.address, self.accounts[0], 6)
        self.rpc.do(o)

        o = self.c.requests(self.address, 6, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        request = self.c.parse_request(r)
        self.assertTrue(request.is_executed())

        o = self.c.last_serial(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        serial = self.c.parse_count(r)
        self.assertEqual(serial, 6)

        o = self.c.next_serial(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        serial = self.c.parse_count(r)
        self.assertEqual(serial, 1)

        c = ERC20(self.chain_spec)
        o = c.balance(self.token_address, self.accounts[0], sender_address=self.accounts[0])
        r = self.rpc.do(o)
        balance = c.parse_balance(r)
        self.assertEqual(balance, 100)

        o = c.balance(self.token_address, self.accounts[5], sender_address=self.accounts[0])
        r = self.rpc.do(o)
        balance = c.parse_balance(r)
        self.assertEqual(balance, 5)

        o = c.balance(self.token_address, self.accounts[9], sender_address=self.accounts[0])
        r = self.rpc.do(o)
        balance = c.parse_balance(r)
        self.assertEqual(balance, 10000-100-5)


    def test_hi_lo_shuffle(self):
        for i in range(100):
            (tx_hash_hex, o) = self.c.create_request(self.address, self.accounts[0], self.accounts[9], self.accounts[(i+1)%8], self.token_address, i+1)
            self.rpc.do(o)

        v = list(range(1, 101))
        c = 0
        random.shuffle(v)
        for i in v:
            logg.info('processing request number {} at index {}'.format(c, i))
            (tx_hash_hex, o) = self.c.nay(self.address, self.accounts[0], i)
            self.rpc.do(o)

            (tx_hash_hex, o) = self.c.check_result(self.address, self.accounts[0], i)
            self.rpc.do(o)
            o = receipt(tx_hash_hex)
            r = self.rpc.do(o)
            self.assertEqual(r['status'], 1)

            o = self.c.requests(self.address, i, sender_address=self.accounts[0])
            r = self.rpc.do(o)
            request = self.c.parse_request(r)
            self.assertTrue(request.is_rejected())

            c += 1
            o = self.c.count(self.address, sender_address=self.accounts[0])
            r = self.rpc.do(o)
            count = self.c.parse_count(r)
            self.assertEqual(count, 100-c)


        o = self.c.last_serial(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        serial = self.c.parse_count(r)
        self.assertEqual(serial, 100)

        o = self.c.next_serial(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        serial = self.c.parse_count(r)
        self.assertEqual(serial, 0)
 

if __name__ == '__main__':
    unittest.main()
