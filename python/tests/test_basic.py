# standard imports
import os
import unittest
import logging

# external imports
from chainlib.eth.nonce import RPCNonceOracle
from chainlib.eth.tx import receipt

# local imports
from erc20_transfer_authorization import TransferAuthorization
from erc20_transfer_authorization.unittest import TestBase

logg = logging.getLogger()

testdir = os.path.dirname(__file__)


class TestBasic(TestBase):

    def setUp(self):
        super(TestBasic, self).setUp()
        nonce_oracle = RPCNonceOracle(self.accounts[0], self.rpc)
        self.c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = self.c.add_writer(self.address, self.accounts[0], self.accounts[1])
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.add_writer(self.address, self.accounts[0], self.accounts[2])
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)


    def test_basic(self):
        c = TransferAuthorization(self.chain_spec)

        o = c.is_writer(self.address, self.accounts[0], sender_address=self.accounts[0])
        r = self.rpc.do(o)
        self.assertTrue(c.parse_signers(r))

        o = c.is_writer(self.address, self.accounts[1], sender_address=self.accounts[0])
        r = self.rpc.do(o)
        self.assertTrue(c.parse_signers(r))

        o = c.is_writer(self.address, self.accounts[2], sender_address=self.accounts[0])
        r = self.rpc.do(o)
        self.assertTrue(c.parse_signers(r))

        o = c.is_writer(self.address, self.accounts[3], sender_address=self.accounts[0])
        r = self.rpc.do(o)
        self.assertFalse(c.parse_signers(r))

        o = c.signer_count(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        count = c.parse_count(r)
        self.assertEqual(count, 3)


#    def test_limit(self):
#        nonce_oracle = RPCNonceOracle(self.accounts[0], self.rpc)
#        c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
#        
#        (tx_hash_hex, o) = c.create_request(self.address, self.accounts[0], self.accounts[1], self.accounts[2], self.token_address, 1024)
#        self.rpc.do(o)
#        o = receipt(tx_hash_hex)
#        r = self.rpc.do(o)
#        self.assertEqual(r['status'], 1)
#
#        (tx_hash_hex, o) = c.create_request(self.address, self.accounts[0], self.accounts[1], self.accounts[2], self.token_address, 1024)
#        self.rpc.do(o)
#        o = receipt(tx_hash_hex)
#        r = self.rpc.do(o)
#        self.assertEqual(r['status'], 1)
#
#        (tx_hash_hex, o) = c.yay(self.address, self.accounts[0], 1)
#        self.rpc.do(o)
#        o = receipt(tx_hash_hex)
#        r = self.rpc.do(o)
#        self.assertEqual(r['status'], 1)
#
#        (tx_hash_hex, o) = c.yay(self.address, self.accounts[0], 2)
#        self.rpc.do(o)
#        o = receipt(tx_hash_hex)
#        r = self.rpc.do(o)
#        self.assertEqual(r['status'], 1)
#
#        (tx_hash_hex, o) = c.check_result(self.address, self.accounts[0], 1)
#        self.rpc.do(o)
#        o = receipt(tx_hash_hex)
#        r = self.rpc.do(o)
#        self.assertEqual(r['status'], 1)
#
#        o = c.requests(self.address, 1, sender_address=self.accounts[0])
#        r = self.rpc.do(o)
#        request = self.c.parse_request(r)
#        logg.debug('request {}'.format(request))
#        self.assertTrue(request.is_accepted())


    def test_serial(self):
        nonce_oracle = RPCNonceOracle(self.accounts[0], self.rpc)
        c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        for i in range(5):
            (tx_hash_hex, o) = c.create_request(self.address, self.accounts[0], self.accounts[i], self.accounts[i+1], self.token_address, 1024 * i)
            self.rpc.do(o)
            o = receipt(tx_hash_hex)
            r = self.rpc.do(o)
            self.assertEqual(r['status'], 1)

        o = c.count(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        count = c.parse_count(r)
        self.assertEqual(count, 5)


if __name__ == '__main__':
    unittest.main()
