# standard imports
import os
import unittest
import logging

# external imports
from chainlib.eth.nonce import RPCNonceOracle
from chainlib.eth.address import is_same_address
from chainlib.eth.tx import receipt

# local imports
from erc20_transfer_authorization import TransferAuthorization
from erc20_transfer_authorization.unittest import TestBase

logg = logging.getLogger()

testdir = os.path.dirname(__file__)


class TestQuorum(TestBase):

    def setUp(self):
        super(TestQuorum, self).setUp()
        nonce_oracle = RPCNonceOracle(self.accounts[0], self.rpc)
        self.c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)

        for i in range(1, 5):
            (tx_hash_hex, o) = self.c.add_writer(self.address, self.accounts[0], self.accounts[i])
            self.rpc.do(o)
            o = receipt(tx_hash_hex)
            r = self.rpc.do(o)
            self.assertEqual(r['status'], 1)


    def test_basic(self):
        o = self.c.quorum_threshold(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        count = self.c.parse_count(r)
        self.assertEqual(count, 1)

        o = self.c.veto_threshold(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        count = self.c.parse_count(r)
        self.assertEqual(count, 0)

        (tx_hash_hex, o) = self.c.set_thresholds(self.address, self.accounts[0], 3, 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        o = self.c.quorum_threshold(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        count = self.c.parse_count(r)
        self.assertEqual(count, 3)

        o = self.c.veto_threshold(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        count = self.c.parse_count(r)
        self.assertEqual(count, 1)


    def test_signer_sanity(self):
        (tx_hash_hex, o) = self.c.set_thresholds(self.address, self.accounts[0], 0, 0)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 0)

        (tx_hash_hex, o) = self.c.set_thresholds(self.address, self.accounts[0], 0, 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 0)

        (tx_hash_hex, o) = self.c.set_thresholds(self.address, self.accounts[0], 5, 0)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.set_thresholds(self.address, self.accounts[0], 6, 0)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 0)

        (tx_hash_hex, o) = self.c.set_thresholds(self.address, self.accounts[0], 5, 5)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.set_thresholds(self.address, self.accounts[0], 5, 6)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 0)


    def test_register(self):
        (tx_hash_hex, o) = self.c.set_thresholds(self.address, self.accounts[0], 2, 2)
        self.rpc.do(o)

        (tx_hash_hex, o) = self.c.create_request(self.address, self.accounts[0], self.accounts[1], self.accounts[2], self.token_address, 1024)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.yay(self.address, self.accounts[0], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        o = self.c.requests(self.address, 1, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        request = self.c.parse_request(r)
        self.assertTrue(is_same_address(request.sender, self.accounts[1]))
        self.assertTrue(is_same_address(request.recipient, self.accounts[2]))
        self.assertTrue(is_same_address(request.token, self.token_address))
        self.assertEqual(request.value, 1024)
        self.assertEqual(request.serial, 1)
        self.assertEqual(request.yay, 1)
        self.assertEqual(request.nay, 0)

        (tx_hash_hex, o) = self.c.check_result(self.address, self.accounts[0], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

    
    def test_veto(self):
        (tx_hash_hex, o) = self.c.set_thresholds(self.address, self.accounts[0], 1, 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.create_request(self.address, self.accounts[0], self.accounts[1], self.accounts[2], self.token_address, 1024)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.nay(self.address, self.accounts[0], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[1], self.rpc)
        c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.yay(self.address, self.accounts[1], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 0)

        o = self.c.requests(self.address, 1, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        request = self.c.parse_request(r)
        self.assertTrue(request.is_rejected())


    def test_simple_yay(self):
        (tx_hash_hex, o) = self.c.set_thresholds(self.address, self.accounts[0], 2, 2)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.create_request(self.address, self.accounts[0], self.accounts[1], self.accounts[2], self.token_address, 1024)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.yay(self.address, self.accounts[0], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[1], self.rpc)
        c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.yay(self.address, self.accounts[1], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        o = self.c.requests(self.address, 1, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        request = self.c.parse_request(r)
        self.assertTrue(request.is_accepted())


    def test_nay_before_yay(self):
        (tx_hash_hex, o) = self.c.set_thresholds(self.address, self.accounts[0], 2, 2)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.create_request(self.address, self.accounts[0], self.accounts[1], self.accounts[2], self.token_address, 1024)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.yay(self.address, self.accounts[0], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[1], self.rpc)
        c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.nay(self.address, self.accounts[1], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[2], self.rpc)
        c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.nay(self.address, self.accounts[2], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        o = self.c.requests(self.address, 1, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        request = self.c.parse_request(r)
        self.assertFalse(request.is_accepted())


    def test_yay_nay_yay(self):
        (tx_hash_hex, o) = self.c.set_thresholds(self.address, self.accounts[0], 2, 2)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.create_request(self.address, self.accounts[0], self.accounts[1], self.accounts[2], self.token_address, 1024)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.yay(self.address, self.accounts[0], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[1], self.rpc)
        c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.nay(self.address, self.accounts[1], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[2], self.rpc)
        c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.yay(self.address, self.accounts[2], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        o = self.c.requests(self.address, 1, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        request = self.c.parse_request(r)
        self.assertTrue(request.is_accepted())


    def test_yay_yay_nay(self):
        (tx_hash_hex, o) = self.c.set_thresholds(self.address, self.accounts[0], 2, 2)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.create_request(self.address, self.accounts[0], self.accounts[1], self.accounts[2], self.token_address, 1024)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.yay(self.address, self.accounts[0], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[1], self.rpc)
        c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.yay(self.address, self.accounts[1], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[2], self.rpc)
        c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.nay(self.address, self.accounts[2], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 0)

        o = self.c.requests(self.address, 1, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        request = self.c.parse_request(r)
        logg.debug('request {}'.format(request))
        self.assertTrue(request.is_accepted())


    def test_nay_yay_nay(self):
        (tx_hash_hex, o) = self.c.set_thresholds(self.address, self.accounts[0], 2, 2)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.create_request(self.address, self.accounts[0], self.accounts[1], self.accounts[2], self.token_address, 1024)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.nay(self.address, self.accounts[0], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[1], self.rpc)
        c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.yay(self.address, self.accounts[1], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[2], self.rpc)
        c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.nay(self.address, self.accounts[2], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        o = self.c.requests(self.address, 1, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        request = self.c.parse_request(r)
        self.assertTrue(request.is_rejected())


    def test_nay_yay_nay_yay(self):
        (tx_hash_hex, o) = self.c.set_thresholds(self.address, self.accounts[0], 2, 2)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.create_request(self.address, self.accounts[0], self.accounts[1], self.accounts[2], self.token_address, 1024)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.nay(self.address, self.accounts[0], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[1], self.rpc)
        c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.yay(self.address, self.accounts[1], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[2], self.rpc)
        c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.nay(self.address, self.accounts[2], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[2], self.rpc)
        c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.yay(self.address, self.accounts[2], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 0)

        o = self.c.requests(self.address, 1, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        request = self.c.parse_request(r)
        logg.debug('request {}'.format(request))
        self.assertTrue(request.is_rejected())


    def test_nay_yay_nay_nay(self):
        (tx_hash_hex, o) = self.c.set_thresholds(self.address, self.accounts[0], 2, 2)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.create_request(self.address, self.accounts[0], self.accounts[1], self.accounts[2], self.token_address, 1024)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.nay(self.address, self.accounts[0], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[1], self.rpc)
        c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.yay(self.address, self.accounts[1], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[2], self.rpc)
        c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.nay(self.address, self.accounts[2], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[2], self.rpc)
        c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.nay(self.address, self.accounts[2], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 0)

        o = self.c.requests(self.address, 1, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        request = self.c.parse_request(r)
        logg.debug('request {}'.format(request))
        self.assertTrue(request.is_rejected())


    def test_nay_majority(self):
        (tx_hash_hex, o) = self.c.set_thresholds(self.address, self.accounts[0], 3, 0)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.create_request(self.address, self.accounts[0], self.accounts[1], self.accounts[2], self.token_address, 1024)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = self.c.nay(self.address, self.accounts[0], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[1], self.rpc)
        c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.nay(self.address, self.accounts[1], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[2], self.rpc)
        c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.nay(self.address, self.accounts[2], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[2], self.rpc)
        c = TransferAuthorization(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.nay(self.address, self.accounts[2], 1)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 0)

        o = self.c.requests(self.address, 1, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        request = self.c.parse_request(r)
        logg.debug('request {}'.format(request))
        self.assertTrue(request.is_rejected())


if __name__ == '__main__':
    unittest.main()
