pragma solidity >0.8.0;

// SPDX-License-Identifier: GPL-3.0-or-later

contract ERC20TransferAuthorization {
	struct Transaction {
		uint256 value;
		address sender;
		address recipient;
		address token;
		uint32 serial;
		uint32 yay;
		uint32 nay;
		//uint256 blockNumber;
		// bit 1: started
		// bit 2: approved
		// bit 3: rejected
		// bit 4: finalized
		// bit 5: transfererror
		uint8 result;
	}

	mapping ( uint32 => mapping ( address => int8 )) public vote;
	//mapping ( uint256 => address[] ) public voters;
	mapping( uint32 => Transaction ) public requests;
	//mapping(address => uint256[]) public requestSenderIndex;
	//mapping(address => uint256[]) public requestRecipientIndex;
	address public owner;
	uint32 hi;
	uint32 lo;
	uint32 public count;
	uint32 public quorum;
	uint32 public vetoThreshold;
	uint32 public signerCount;

	mapping(address => bool) public signers;
	address[] public writers;

	event NewRequest(address indexed _sender, address indexed _recipient, address indexed _token, uint256 _value, uint32 _serial);
	event Executed(uint32 _serial);
	event TransferFail(uint32 _serial);
	event QuorumSet(uint32 indexed _quorum, uint32 indexed _vetoThreshold, uint32 indexed _signerCount);
	event WriterAdded(address _signer);
	event WriterRemoved(address _signer);
	event Vetoed(uint32 indexed _serial, uint32 indexed _yays, uint32 indexed _nays);
	event Approved(uint32 indexed _serial, uint32 indexed _yays, uint32 indexed _nays);
	event Rejected(uint32 indexed _serial, uint32 indexed _yays, uint32 indexed _nays);

	constructor() public {
		owner = msg.sender;
		hi = 1;
		lo = 1;
		addWriter(msg.sender);
		setThresholds(1, 0);
	}

	function isWriter(address _signer) public view returns (bool) {
		return signers[_signer];
	}

	function addWriter(address _signer) public returns (uint32) {
		require(msg.sender == owner, 'ERR_ACCESS');
		require(signers[_signer] == false, 'ERR_NOTFOUND');

		signers[_signer] = true;
		signerCount++;
		emit WriterAdded(_signer);
		return signerCount;
	}

	function removeWriter(address _signer) public returns (uint32) {
		//require(msg.sender == owner || msg.sender == _signer, 'ERR_ACCESS');
		require(msg.sender == owner, 'ERR_ACCESS');
		require(signers[_signer] == true, 'ERR_NOTFOUND');
		require(signerCount > quorum && signerCount > vetoThreshold, 'ERR_REDUCE_THRESHOLD_FIRST');

		signers[_signer] = false;
		signerCount--;
		emit WriterRemoved(_signer);
		return signerCount;
	}

	function setThresholds(uint32 _quorum, uint32 _vetoThreshold) public returns (bool) {
		require(_quorum <= signerCount);
		require(_quorum > 0);
		require(_vetoThreshold <= signerCount);

		quorum = _quorum;
		vetoThreshold = _vetoThreshold;
		emit QuorumSet(quorum, vetoThreshold, signerCount);
		return true;
	}

	// create new request
	function createRequest(address _sender, address _recipient, address _token, uint256 _value) public returns (uint32) {
		Transaction storage txx = requests[hi];

		txx.serial = hi;
		txx.recipient = _recipient;
		txx.sender = _sender;
		txx.token = _token;
		txx.value = _value;
		txx.result = 1;

		count++;
		hi++;

		emit NewRequest(txx.sender, txx.recipient, txx.token, txx.value, txx.serial);

		return txx.serial;
	}

	// if request was oldest in index, move the pointer to oldest request to next oldest unfinished request.
	// if no unfinished requests exits, it will point to newest request
	function removeItem(uint32 _serialToRemove) private returns (uint32) {
		count--;

		if (count > 0) {
			if (_serialToRemove == lo) {
				uint32 i;
				i = getSerialAt(0);
				if (i == 0) {
					lo = hi;
				} else {
					lo = i;
				}
			}
		} else if (lo != hi) {
			lo = hi;
		}

		return lo;
	}

	// index of newest vote
	function lastSerial() public view returns ( uint32 ) {
		return hi - 1;
	}

	// index of oldest unfinished vote
	function nextSerial() public view returns ( uint32 ) {
		if (hi - lo == 0) {
			if (hi == 1) {
				return 0;
			}
			Transaction storage txx = requests[lo];
			if (txx.result > 0) {
				return lo;
			}
			return 0;
		}
		return lo;
	}

	// get the nth unfinished vote, where nth is _idx, starting at 0
	function getSerialAt(uint32 _idx) public view returns ( uint32 ) {
		uint32 i;

		if (lo == hi) {
			return 0;
		}

		for (uint32 j = lo; j < hi; j++) {
			Transaction storage txx = requests[j];
			if (txx.result & 7 == 1) {
				if (i == _idx) {
					return txx.serial;
				}
				i++;
			}
		}
		return 0;
	}

	// vote yay, one per signer
	function yay(uint32 _serial) public returns (uint32) {
		require(signers[msg.sender], 'ERR_ACCESS');
		require(vote[_serial][msg.sender] == 0, 'ERR_ALREADYVOTED');

		Transaction storage txx = requests[_serial];
		require(txx.result == 1);

		vote[txx.serial][msg.sender] = 1;
		//voters[txx.serial].push(msg.sender);
		txx.yay++;

		checkResult(txx.serial);	

		return txx.yay;
	}

	// vote nay, one per signer
	function nay(uint32 _serial) public returns (uint32) {
		require(signers[msg.sender], 'ERR_ACCESS');
		require(vote[_serial][msg.sender] == 0, 'ERR_ALREADYVOTED');

		Transaction storage txx = requests[_serial];
		require(txx.result == 1);

		vote[txx.serial][msg.sender] = -1;
		//voters[txx.serial].push(msg.sender);
		txx.nay++;

		checkResult(txx.serial);	

		return txx.nay;
	}

	// locks the state of the vote if quorum or veto is reached
	// returns true if state changes
	function checkResult(uint32 _serial) public returns (bool) {
		bool result;
		Transaction storage txx = requests[_serial];

		if (txx.result < 1 || txx.result & 6 > 0) {
			return result;
		}

		if (txx.yay >= quorum) {
			txx.result |= 2;
			emit Approved(txx.serial, txx.yay, txx.nay);
			result = true;
		} else if (vetoThreshold > 0 && txx.nay >= vetoThreshold) {
			txx.result |= 4;
			removeItem(txx.serial);
			emit Vetoed(txx.serial, txx.yay, txx.nay);
			result = true;
		} else if (signerCount - txx.nay < quorum) { 
			txx.result |= 4;
			removeItem(txx.serial);
			emit Rejected(txx.serial, txx.yay, txx.nay);
			result = true;
		}

		return result;
	}

	// execute transfer. needs positive vote result
	function executeRequest(uint32 _serial) public returns (bool) {
		Transaction storage txx = requests[_serial];

		require(txx.serial > 0, 'ERR_INVALID_REQUEST');
		require(txx.result & 11 == 3, 'ERR_NOT_ENDORSED');

		removeItem(txx.serial);
		txx.result |= 8;

		(bool success, bytes memory _r) = txx.token.call(abi.encodeWithSignature("transferFrom(address,address,uint256)", txx.sender, txx.recipient, txx.value));

		//txx.blockNumber = block.number;
		//requestSenderIndex[txx.sender].push(txx.serial);
		//requestRecipientIndex[txx.recipient].push(txx.serial);
		if (success) {
			emit Executed(_serial);
		} else {
			txx.result |= 16; // this edit is for convenience only. since bit 4 is already set, it is not re-entrant.
			emit TransferFail(_serial);
		}

		return success;
	}
}
