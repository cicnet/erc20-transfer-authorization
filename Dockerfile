FROM grassrootseconomics:bancor

ARG pip_extra_index_url=https://pip.grassrootseconomics.net:8433

COPY ./python/ ./erc20-approval-escrow/

RUN cd erc20-approval-escrow && \
	pip install --extra-index-url $pip_extra_index_url .

